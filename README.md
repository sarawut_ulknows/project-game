# About

Project game is my course project.

It's a RPG game in command-line.

## Future of this game
- Guide to all player
- Level progression
- Attack and Defend will effect to player
- Monster name and Map will random
- Stats of player is random.
- Save and Load profile.

## Requirements
- Window screen with 20 lines.
- Dart language has installed.
