import '../FileControl.dart';
import 'FileStats.dart';

class PlayerDefault extends FileStats with FileControl{

  final int _healthPoint = 100;
  final int _attackPoint = 50;
  final int _defendPoint = 50;
  final int _specialAtkCooldown = 3;
  final int _healingCooldown = 3;
  final int _maxLevel = 100;

  @override
  int get healthPoint {
    var temp = getSpecificData('\\data\\default_player.data', 'Health_Point');
    if(temp == 'error'){
      print('Error! No attribute \"Health_Point\" found!');
      return _healthPoint;
    }else{
      return int.parse(temp);
    }
  }

  @override
  int get attackPoint {
    var temp = getSpecificData('\\data\\default_player.data', 'Attack_Point');
    if(temp == 'error'){
      print('Error! No attribute \"Attack_Point\" found!');
      return _attackPoint;
    }else{
      return int.parse(temp);
    }
  }

  @override
  int get defendPoint {
    var temp = getSpecificData('\\data\\default_player.data', 'Defend_Point');
    if(temp == 'error'){
      print('Error! No attribute \"Attack_Point\" found!');
      return _defendPoint;
    }else{
      return int.parse(temp);
    }
  }

  int get spCooldown {
    var temp = getSpecificData('\\data\\default_player.data', 'SpecialAttack_Skill_Cooldown');
    if(temp == 'error'){
      print('Error! No attribute \"SpecialAttack_Skill_Cooldown\" found!');
      return _specialAtkCooldown;
    }else{
      return int.parse(temp);
    }
  }

  int get healCooldown {
    var temp = getSpecificData('\\data\\default_player.data', 'Healing_Skill_Cooldown');
    if(temp == 'error'){
      print('Error! No attribute \"Healing_Skill_Cooldown\" found!');
      return _healingCooldown;
    }else{
      return int.parse(temp);
    }
  }

  int get maxLevel {
    var temp = getSpecificData('\\data\\default_player.data', 'Max_Level');
    if(temp == 'error'){
      print('Error! No attribute \"Max_Level\" found!');
      return _maxLevel;
    }else{
      return int.parse(temp);
    }
  }
}
