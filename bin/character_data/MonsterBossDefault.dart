import '../FileControl.dart';
import 'FileStats.dart';

class MonsterBossDefault extends FileStats with FileControl{
  final int _healthPoint = 80;
  final int _attackPoint = 40;
  final int _defendPoint = 40;
  final int _healthPointRandom = 25;
  final int _attackPointRandom = 17;
  final int _defendPointRandom = 17;

  @override
  int get healthPoint {
    var temp = getSpecificData('\\data\\default_monster_boss.data', 'Health_Point');
    if(temp == 'error'){
      print('Error! No attribute \"Health_Point\" found!');
      return _healthPoint;
    }else{
      return int.parse(temp);
    }
  }

  @override
  int get attackPoint {
    var temp = getSpecificData('\\data\\default_monster_boss.data', 'Attack_Point');
    if(temp == 'error'){
      print('Error! No attribute \"Attack_Point\" found!');
      return _attackPoint;
    }else{
      return int.parse(temp);
    }
  }

  @override
  int get defendPoint {
    var temp = getSpecificData('\\data\\default_monster_boss.data', 'Defend_Point');
    if(temp == 'error'){
      print('Error! No attribute \"Defend_Point\" found!');
      return _defendPoint;
    }else{
      return int.parse(temp);
    }
  }

  int get healthPointRandom {
    var temp = getSpecificData('\\data\\default_monster_boss.data', 'Health_Point_Range');
    if(temp == 'error'){
      print('Error! No attribute \"Health_Point_Range\" found!');
      return _healthPointRandom;
    }else{
      return int.parse(temp);
    }
  }

  int get attackPointRandom {
    var temp = getSpecificData('\\data\\default_monster_boss.data', 'Attack_Point_Range');
    if(temp == 'error'){
      print('Error! No attribute \"Attack_Point_Range\" found!');
      return _attackPointRandom;
    }else{
      return int.parse(temp);
    }
  }

  int get defendPointRandom {
    var temp = getSpecificData('\\data\\default_monster_boss.data', 'Defend_Point_Range');
    if(temp == 'error'){
      print('Error! No attribute \"Defend_Point_Range\" found!');
      return _defendPointRandom;
    }else{
      return int.parse(temp);
    }
  }
}
