abstract class FileStats{
  int get healthPoint;

  int get attackPoint;

  int get defendPoint;
}