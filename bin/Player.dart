import 'dart:io';
import 'dart:math';

import 'character_data/PlayerDefault.dart';
import 'ActionBase.dart';
import 'profile/RWProfilePlayer.dart';

class Player extends stats {
  //set default stats
  final _defaultPlayer = PlayerDefault();
  final _rwProfile = RWProfilePlayer();

  //name
  late String _name;

  //level
  int _level = 1;
  late final int _maxLevel = _defaultPlayer.maxLevel;
  int _xp = 0;
  int _xpMax = 500;

  //stats
  late int _healthPoint;
  late int _healthPointMax;
  late int _attackPoint;
  late int _defendPoint;

  //Cooldown require
  late final int _specialAtkCooldown = _defaultPlayer.spCooldown;
  int _specialAtkCurrentCooldown = 0;

  late final int _healingCooldown = _defaultPlayer.healCooldown;
  int _healingCurrentCooldown = 0;

  // set character
  Player(bool loaded) {
    if (loaded) {
      if (_rwProfile.checkData() && _rwProfile.checkNull()) {
        _loadStats();
        _healthPoint = _healthPointMax;
        getStats();
      } else {
        print('Error to load data your character will reset');
        setName();
        _randomStats();
      }
    } else {
      setName();
      _randomStats();
    }
  }

  void saveData() {
    var listSave = List<String>.empty(growable: true);
    listSave.add('[ Profile saving ]');
    listSave.add('Name = ' + _name);
    listSave.add('Level = ' + _level.toString());
    listSave.add('XP = ' + _xp.toString());
    listSave.add('XP_Level = ' + _xpMax.toString());
    listSave.add('Health_Point = ' + _healthPointMax.toString());
    listSave.add('Attack_Point = ' + _attackPoint.toString());
    listSave.add('Defend_Point = ' + _defendPoint.toString());

    _rwProfile.save(listSave);
  }

  void _loadStats() {
    _name = _rwProfile.name;
    _level = _rwProfile.level;
    _xp = _rwProfile.xp;
    _xpMax = _rwProfile.xpMax;
    _healthPointMax = _rwProfile.healthPoint;
    _attackPoint = _rwProfile.attackPoint;
    _defendPoint = _rwProfile.defendPoint;
  }

  void setName() {
    stdout.write('Input your character name : ');
    _name = stdin.readLineSync()!;
  }

  // stats setting
  void _randomStats() {
    var rng = Random();
    var range = 50;
    var rand = rng.nextInt(range);
    _setStats(range, rand);
  }

  // config stats
  void _setStats(int range, int rand) {
    if (rand > range / 2) {
      _healthPointMax =
          _defaultPlayer.healthPoint + (rand - (range / 2)).toInt();
      _healthPoint = _healthPointMax;
      _attackPoint = _defaultPlayer.attackPoint -
          (((_healthPointMax - _defaultPlayer.healthPoint) / 100) *
                  _defaultPlayer.attackPoint)
              .toInt();
      _defendPoint = _defaultPlayer.defendPoint +
          (_defaultPlayer.attackPoint - _attackPoint);
    } else if (rand < range / 2) {
      rand = (range ~/ 2) - rand;
      _healthPointMax = _defaultPlayer.healthPoint - rand;
      _healthPoint = _healthPointMax;
      _attackPoint = _defaultPlayer.attackPoint +
          (((_defaultPlayer.healthPoint - _healthPoint) / 100) *
                  _defaultPlayer.attackPoint)
              .toInt();
      _defendPoint = _defaultPlayer.defendPoint -
          (_attackPoint - _defaultPlayer.attackPoint);
    } else {
      _healthPointMax = _defaultPlayer.healthPoint;
      _healthPoint = _healthPointMax;
      _attackPoint = _defaultPlayer.attackPoint;
      _defendPoint = _defaultPlayer.defendPoint;
    }
  }

  // level and xp
  int get getCurrentXP => _xp;

  int get getMaxXP => _xpMax;

  int get getLevel => _level;

  // mock data
  void set() {
    _healthPointMax = 100;
    _healthPoint = _healthPointMax;
    _attackPoint = 50;
    _defendPoint = 60;
    receivingXP(100000000000);
  }

  //action
  @override
  int get attack => _attackPoint;

  void resetCooldown() {
    _specialAtkCurrentCooldown = 0;
    _healingCurrentCooldown = 0;
  }

  //special action
  int specialAttack() {
    _specialAtkCurrentCooldown = _specialAtkCooldown + 1;
    return _attackPoint + ((_attackPoint / 100) * 25).round();
  }

  //when no cooldown return false and have cooldown return true
  bool checkSpecialAttackCooldownAvailable() {
    if (_specialAtkCurrentCooldown != 0) {
      return false;
    } else {
      return true;
    }
  }

  // cooldown special attack reduce after turn end
  void reduceSACooldown() {
    if (_specialAtkCurrentCooldown != 0) {
      _specialAtkCurrentCooldown--;
    } else {
      return;
    }
  }

  int get getSACrCooldown => _specialAtkCurrentCooldown;

  int get getSACooldown => _specialAtkCooldown;

  //healing
  void healing() {
    _healingCurrentCooldown = _healingCooldown + 1;
    var healAmount = ((_healthPointMax / 100) * 40).round();
    if (_healthPoint + healAmount > _healthPointMax) {
      print(
          'Healing : ' + (_healthPointMax - _healthPoint).toString() + ' hp.');
      _healthPoint = _healthPointMax;
      return;
    } else {
      print('Healing : $healAmount');
      _healthPoint = _healthPoint + healAmount;
      return;
    }
  }

  //when no cooldown return false and have cooldown return true
  bool checkHealingCooldownAvailable() {
    if (_healingCurrentCooldown != 0) {
      return false;
    } else {
      return true;
    }
  }

  // cooldown healing reduce after turn end
  void reduceHealingCooldown() {
    if (_healingCurrentCooldown != 0) {
      _healingCurrentCooldown--;
    } else {
      return;
    }
  }

  // return cooldown special attack
  int get getHealingCrCooldown => _healingCurrentCooldown;

  int get getHealingCooldown => _healingCooldown;

  //Revive
  void revive() {
    _healthPoint = _healthPointMax;
  }

  // defending damage from monster
  @override
  bool defend(int atkDamage) {
    var baseDmg = 80;
    var lastPercent = 20;
    if (atkDamage > _defendPoint) {
      baseDmg =
          baseDmg - ((atkDamage - _defendPoint) / (atkDamage / 100)).round();
    } else if (atkDamage < _defendPoint) {
      baseDmg = baseDmg +
          (((_defendPoint - atkDamage) / (atkDamage / 2)) * lastPercent)
              .round();
    }

    var rng = Random();
    var dmg = rng.nextInt(15);

    print('---------------');
    if (baseDmg + dmg < 100) {
      return reduceHP(
          atkDamage - ((atkDamage / 100) * (baseDmg + dmg)).round());
    } else {
      print('Attack miss!');
      print('---------------');
      return false;
    }
  }

  // reduce hp by damage
  @override
  bool reduceHP(int amount) {
    print('$_name receive $amount damage(s).');
    if (_healthPoint - amount > 0) {
      // Not die
      _healthPoint = _healthPoint - amount;
      print('HP : $_healthPoint / $_healthPointMax');
      print('---------------');
      return false;
    } else {
      // die
      _playerDead();
      _xp = 0;
      _healthPoint = _healthPointMax;
      return true;
    }
  }

  // when player dead
  void _playerDead() {
    print('     You are dead!');
    print('You are lose $_xp.');
    print('Becareful! your xp will gone if you die.');
  }

  // xp receive
  void receivingXP(int receiveXP) {
    if (_level <= _maxLevel) {
      if (_xp + receiveXP < _xpMax) {
        _xp = receiveXP + _xp;
        receiveXP = 0;
      } else {
        while (_xp + receiveXP >= _xpMax) {
          _xp = (receiveXP + _xp) - _xpMax;
          _checkLevelUP(_xp);
          receiveXP = 0;
        }
      }
    }
  }

  // when monster is defeated, player got some xp
  void _checkLevelUP(int currentXP) {
    if (_level <= _maxLevel) {
      _xpMax = (_xpMax * (1.5 - ((_level / 2) / 100))).toInt();
      _level++;
    } else {
      _xpMax = 0;
    }

    //stats
    var lastHealthPoint = _healthPointMax;
    var lastAttackPoint = _attackPoint;
    var lastDefendPoint = _defendPoint;
    _healthPointMax = _healthPointMax + ((_healthPointMax / 100) * 14).toInt();
    _attackPoint = _attackPoint + ((_attackPoint / 100) * 14).toInt();
    _defendPoint = _defendPoint + ((_defendPoint / 100) * 14).toInt();
    _showLevelUP(
        lastHealthPoint, lastAttackPoint, lastDefendPoint, _xpMax, currentXP);
  }

  // when player level up
  void _showLevelUP(int lasthealthPoint, int lastAttackPoint,
      int lastDefendPoint, int xpRequire, int currentXP) {
    print('\n\n     Level up!');
    print('You are level ' + _level.toString() + ' ($currentXP/$xpRequire)');
    print('Your stats increase!');
    print('-HealthPoint increase from ' +
        lasthealthPoint.toString() +
        ' to ' +
        _healthPointMax.toString());
    print('-Attack increase form ' +
        lastAttackPoint.toString() +
        ' to ' +
        _attackPoint.toString());
    print('-Defend increase from ' +
        lastDefendPoint.toString() +
        ' to ' +
        _defendPoint.toString() +
        '\n\n');
  }

  // show xp
  void get getXP => print('XP : $_xp / $_xpMax');

  // show stats
  @override
  void getStats() {
    print('\n\nYour stats');
    print('Name : $_name');
    print('Level : $_level ($_xp / $_xpMax)');
    print('HP : $_healthPoint / $_healthPointMax');
    print('ATK : $_attackPoint');
    print('DEF : $_defendPoint');
    print('\n');
  }
}
