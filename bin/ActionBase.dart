abstract class stats{
  bool defend(int atkDamage);

  bool reduceHP(int amount);

  void getStats();

  int get attack;
}