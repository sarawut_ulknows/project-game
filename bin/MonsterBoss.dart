import 'dart:math';

import 'FileControl.dart';
import 'character_data/MonsterBossDefault.dart';
import 'ActionBase.dart';

class MonsterBoss extends stats with FileControl{
  //set default stats
  final _defaultMonsterBoss = MonsterBossDefault();

  //name
  late String _name;

  // level
  late int _level;

  // stats
  late int _healthPointMax;
  late int _healthPoint;
  late int _attackPoint;
  late int _defendPoint;

  // running
  MonsterBoss(int playerLevel) {
    _name = randomData('data\\monster.data');
    _level = _randomLevel(playerLevel);
    _randomStats(_level);
    _healthPoint = _healthPointMax;
    getStats();
  }

  int _randomLevel(int playerLevel) {
    var rng = Random();
    var monsterLevel;
    if (playerLevel < 6) {
      monsterLevel = playerLevel;
      return 1;
    } else {
      monsterLevel = (playerLevel - 5) + rng.nextInt(6);
      return monsterLevel;
    }
  }

  void _randomStats(int playerLevel) {
    var rng = Random();
    _healthPointMax = rng.nextInt(_defaultMonsterBoss.healthPointRandom) + _defaultMonsterBoss.healthPoint;
    _attackPoint = rng.nextInt(_defaultMonsterBoss.attackPointRandom) + _defaultMonsterBoss.attackPoint;
    _defendPoint = rng.nextInt(_defaultMonsterBoss.defendPointRandom) + _defaultMonsterBoss.defendPoint;

    for (var i = 2; i <= playerLevel; i++) {
      _healthPointMax = _healthPointMax + ((_healthPointMax / 100) * 14).toInt();
      _attackPoint = _attackPoint + ((_attackPoint / 100) * 14).toInt();
      _defendPoint = _defendPoint + ((_defendPoint / 100) * 14).toInt();
    }
  }

  @override
  int get attack => _attackPoint;

  int get getHP => _healthPoint;

  int droppedXP(int playerXP) {
    var rng = Random();
    var rand = ((playerXP / 100) * (rng.nextInt(15) + 10)*1000).round().toInt();

    print('You are got $rand xp!');
    return rand;
  }

  @override
  bool defend(int atkPlayer) {
    var baseReduceDMGPercent = 50;
    var percentDmg = ((atkPlayer / _defendPoint) * 100).round();

    /**atk/def = 1+ , 1 , 1-
     * > 1 percent increase. / atk > def
     * = 1 range 15-30. / atk = def
     * < 1 recent decrease. / atk < def
     */
    if (percentDmg > 1) {
      baseReduceDMGPercent = baseReduceDMGPercent -
          (((atkPlayer - _defendPoint) / (_defendPoint / 100)).round() *
                  ((100 - baseReduceDMGPercent) / 100))
              .round();
    } else if (percentDmg < 1) {
      baseReduceDMGPercent = baseReduceDMGPercent +
          (((atkPlayer - _defendPoint) / (_defendPoint / 100)).round() *
                  ((100 - baseReduceDMGPercent) / 100))
              .round();
    }

    var rng = Random();
    var randomReduceDMGPercent = rng.nextInt(15);
    //rng.nextInt(15)

    print('\n\n---------------');
    if (baseReduceDMGPercent + randomReduceDMGPercent < 100) {
      return reduceHP(
          ((atkPlayer / 100) * (100 - (baseReduceDMGPercent))).round());
    } else {
      print('Attack miss!');
      return false;
    }
  }

  @override
  bool reduceHP(int amount) {
    print('$_name take ' + amount.toString() + ' damage(s).');
    if (_healthPoint - amount > 0) {
      // Not die
      _healthPoint = _healthPoint - amount;
      print('HP : $_healthPoint / $_healthPointMax');
      return false;
    } else {
      // die
      print('Monster $_name has defeated!');
      return true;
    }
  }

  @override
  void getStats() {
    print('\nBoss : $_name');
    print('Level : $_level');
    print('HP : $_healthPoint / $_healthPointMax');
    print('ATK : $_attackPoint');
    print('DEF : $_defendPoint \n');
  }
}
