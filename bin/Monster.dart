import 'dart:math';

import 'FileControl.dart';
import 'character_data/MonsterDefault.dart';
import 'ActionBase.dart';

class Monster extends stats with FileControl{
  //set default stats
  final _defaultMonster = MonsterDefault();

  //name
  late String _name;

  // level
  late int _level;

  // stats
  late int _healthPointMax;
  late int _healthPoint;
  late int _attackPoint;
  late int _defendPoint;

  // running
  Monster(int playerLevel) {
    _name = randomData('data\\monster.data');
    _level = _randomLevel(playerLevel);
    _randomStats(_level);
    _healthPoint = _healthPointMax;
    getStats();
  }

  int _randomLevel(int playerLevel) {
    var rng = Random();
    var monsterLevel;
    if (playerLevel < 6) {
      monsterLevel = playerLevel;
      return 1;
    } else {
      monsterLevel = (playerLevel - 5) + rng.nextInt(6);
      return monsterLevel;
    }
  }

  void _randomStats(int playerLevel) {
    var rng = Random();
    _healthPointMax = rng.nextInt(_defaultMonster.healthPointRandom) + _defaultMonster.healthPoint;
    _attackPoint = rng.nextInt(_defaultMonster.attackPointRandom) + _defaultMonster.attackPoint;
    _defendPoint = rng.nextInt(_defaultMonster.defendPointRandom) + _defaultMonster.defendPoint;

    for (var i = 2; i <= playerLevel; i++) {
      _healthPointMax = _healthPointMax + ((_healthPointMax / 100) * 14).toInt();
      _attackPoint = _attackPoint + ((_attackPoint / 100) * 14).toInt();
      _defendPoint = _defendPoint + ((_defendPoint / 100) * 14).toInt();
    }
  }

  //action
  @override
  int get attack => _attackPoint;

  int get getHP => _healthPoint;

  // defend damage from player
  @override
  bool defend(int atkDamage) {
    var baseReduceDMGPercent = 50;
    var percentDmg = ((atkDamage / _defendPoint) * 100).round();

    /*
     * atk/def = 1+ , 1 , 1-
     * > 1 percent increase. / atk > def
     * = 1 range 15-30. / atk = def
     * < 1 recent decrease. / atk < def
     */
    if (percentDmg > 1) {
      baseReduceDMGPercent = baseReduceDMGPercent -
          (((atkDamage - _defendPoint) / (_defendPoint / 100)).round() *
                  ((100 - baseReduceDMGPercent) / 100))
              .round();
    } else if (percentDmg < 1) {
      baseReduceDMGPercent = baseReduceDMGPercent +
          (((atkDamage - _defendPoint) / (_defendPoint / 100)).round() *
                  ((100 - baseReduceDMGPercent) / 100))
              .round();
    }

    var rng = Random();
    var randomReduceDMGPercent = rng.nextInt(15);

    // check damage is more than 0
    print('\n\n---------------');
    if (baseReduceDMGPercent + randomReduceDMGPercent < 100) {
      return reduceHP(
          ((atkDamage / 100) * (100 - (baseReduceDMGPercent))).round());
    } else {
      print('Attack miss!');
      return false;
    }
  }

  // damage to hp
  @override
  bool reduceHP(int amount) {
    print('$_name take ' + amount.toString() + ' damage(s).');
    if (_healthPoint - amount > 0) {
      // Not die
      _healthPoint = _healthPoint - amount;
      print('HP : $_healthPoint / $_healthPointMax');
      return false;
    } else {
      // die
      print('Monster $_name has defeated!');
      return true;
    }
  }

  // monster defeated
  int droppedXP(int playerMaxXP) {
    var rng = Random();
    var rand = ((playerMaxXP / 100) * (rng.nextInt(15) + 10) * 1000).round();
    print('You are got $rand xp!');
    return rand;
  }

  // show stats
  @override
  void getStats() {
    print('\nMonster : $_name');
    print('Level : $_level');
    print('HP : $_healthPoint / $_healthPointMax');
    print('ATK : $_attackPoint');
    print('DEF : $_defendPoint \n');
  }
}
