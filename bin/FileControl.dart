import 'dart:io';

import 'dart:math';

mixin FileControl {
  var directoryFile = Directory.current.path + '\\bin\\';

  int lineFile(String fileName) {
    var path = directoryFile + fileName;
    try {
      var file = File(path);
      return file.readAsLinesSync().length;
    } catch (e) {
      return 0;
    }
  }

  List<dynamic> getAllListData(String fileName) {
    var path = directoryFile + fileName;
    try {
      var file = File(path);
      return file.readAsLinesSync().toList();
    } catch (e) {
      return [];
    }
  }

  String randomData(String fileName) {
    var rng = Random();
    var rand = rng.nextInt(lineFile(fileName) - 1);

    return getAllListData(fileName)[rand];
  }

  bool writingFile(String fileName, String text) {
    var temp = getAllListData(fileName);
    var path = directoryFile + fileName;
    try {
      var file = File(path);
      var writer = file.openWrite();

      temp.forEach((word) {
        writer.writeln(word);
        print(word);
      });
      writer.writeln('$text');

      writer.close();
      return true;
    } catch (e) {
      return false;
    }
  }

  bool reWritingFile(String fileName, List<String> text) {
    var path = directoryFile + fileName;
    try {
      var file = File(path);
      var writer = file.openWrite();

      text.forEach((stats) { 
        writer.writeln(stats);
      });

      writer.close();
      return true;
    } catch (e) {
      return false;
    }
  }

  String getSpecificData(String fileName, String tagName) {
    var tempList = getAllListData(fileName);
    var tempWord;
    try {
      tempList.forEach((data) {
        if (data.toString().length > tagName.length) {
          if (data.toString().substring(0, tagName.length+1) == tagName + ' ') {
            tempWord = data.toString();
            tempWord = tempWord.replaceAll(tagName + ' = ', '');
          }
        }
      });
      return tempWord;
    } catch (e) {
      return 'error';
    }
  }
}
