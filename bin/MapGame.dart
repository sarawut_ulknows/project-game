import 'dart:math';

import 'FileControl.dart';

class MapGame with FileControl {
  // map
  late String map = randomData('data\\maps.data');
  // stage ( 5/20 | currentStage / stageLength)
  int currentStage = 1;
  late int stageLength;

  String get getCurrentMap => map;

  int get getCurrentStage => currentStage;

  int get getStageLength => stageLength;

  void stageUP() => currentStage++;

  void guideMap() {
    map = 'Virtual Map';
    stageLength = 1;
  }

  bool stageContinue(){
    if(currentStage < stageLength){
      return true;
    }else if(currentStage == stageLength){
      return true;
    }else{
      return false;
    }
  }

  bool lastStage(){
    if(currentStage == stageLength){
      return true;
    }else{
      return false;
    }
  }

  void stageLengthSet(int playerLevel) {
    var rng = Random();
    if (playerLevel < 5) {
      stageLength = 2;
    } else {
      stageLength = rng.nextInt(playerLevel ~/ 4) + 2;
    }
  }
}
