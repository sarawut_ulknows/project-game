import 'dart:io';
import 'Process.dart';

class UI {
  final _mainGame = Process();

  void run() {
    process();
  }

  void process() {
    welcomeScreen();
    var menu = stdin.readLineSync()!;
    while (true) {
      if (menu == '1') {
        _mainGame.load(true);
        break;
      } else if (menu == '2') {
        _mainGame.load(false);
        break;
      } else {
        print('Input mismatch please input again.');
      }
    }

    // _mainGame.guide();

    _mainGame.environment();
    // guide();
  }

  /*
   *  Screen limit 70
   *  
   */
  void welcomeScreen() {
    // spaceNewLine(5);
    // spaceBlank(12);
    print('Welcome to Monster Slayer game.\n');
    // spaceBlank(13);
    print('Press any key to start game.');
    // spaceBlank(19);
    print('(1) Load your save data.');
    // spaceBlank(19);
    print('(2) New game.');
    // spaceNewLine(5);
  }

  void spaceBlank(int timeSpace) {
    for (var i = 0; i < timeSpace; i++) {
      stdout.write(' ');
    }
  }

  void spaceNewLine(int timeEnter) {
    for (var i = 0; i < timeEnter; i++) {
      print('');
    }
  }

  void delay(int timesDelays) {
    sleep(Duration(seconds: timesDelays));
  }

  void guide() {
    print('Guide...');
    print('Welcome to Monster Slayer game.');
    print('This game is turn base game.');
    print('In one map is have many stage.');
    print('In one stage is have many monster');
    print('Last stage of map Boss will appear.');
    print('And you shouldn\'t let health point to zero.');
    print('You will lose all xp but level is not lose.');
  }
}
