import '../FileControl.dart';
import '../character_data/FileStats.dart';

class RWProfilePlayer extends FileStats with FileControl {
  final String _name = '';
  final int _level = 0;
  final int _xp = 0;
  final int _xpMax = 0;
  final int _healthPoint = 0;
  final int _attackPoint = 0;
  final int _defendPoint = 0;

  void save(List<String> data){
    reWritingFile('\\profile\\player_profile.data', data);
  }

  bool checkData() {
    if (name == '') {
      return false;
    }
    if (level == 0) {
      return false;
    }
    if (xp == 0) {
      return false;
    }
    if (xpMax == 0) {
      return false;
    }
    if (healthPoint == 0) {
      return false;
    }
    if (attackPoint == 0) {
      return false;
    }
    if (defendPoint == 0) {
      return false;
    }
    return true;
  }

  bool checkNull(){
    try{
      name;
      level;
      xp;
      xpMax;
      healthPoint;
      attackPoint;
      defendPoint;
      return true;
    }catch(e){
      return false;
    }
  }

  void test() {
    try {
      if (checkData()) {
        print(name);
        print(level);
        print(xp);
        print(xpMax);
        print(healthPoint);
        print(attackPoint);
        print(defendPoint);
      } else {
        print('error to');
      }
    } catch (e) {
      print('error');
    }
  }

  @override
  int get healthPoint {
    var temp = getSpecificData('\\profile\\player_profile.data', 'Health_Point');
    if (temp == 'error') {
      return _healthPoint;
    } else {
      return int.parse(temp);
    }
  }

  @override
  int get attackPoint {
    var temp = getSpecificData('\\profile\\player_profile.data', 'Attack_Point');
    if (temp == 'error') {
      return _attackPoint;
    } else {
      return int.parse(temp);
    }
  }

  @override
  int get defendPoint {
    var temp = getSpecificData('\\profile\\player_profile.data', 'Defend_Point');
    if (temp == 'error') {
      return _defendPoint;
    } else {
      return int.parse(temp);
    }
  }

  String get name {
    var temp = getSpecificData('\\profile\\player_profile.data', 'Name');
    if (temp == 'error') {
      return _name;
    } else {
      return temp;
    }
  }

  int get level {
    var temp = getSpecificData('\\profile\\player_profile.data', 'Level');
    if (temp == 'error') {
      return _level;
    } else {
      return int.parse(temp);
    }
  }

  int get xp {
    var temp = getSpecificData('\\profile\\player_profile.data', 'XP');
    if (temp == 'error') {
      return _xp;
    } else {
      return int.parse(temp);
    }
  }

  int get xpMax {
    var temp = getSpecificData('\\profile\\player_profile.data', 'XP_Level');
    if (temp == 'error') {
      return _xpMax;
    } else {
      return int.parse(temp);
    }
  }
}
