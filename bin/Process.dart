import 'MapGame.dart';
import 'Monster.dart';
import 'MonsterBoss.dart';
import 'Player.dart';
import 'dart:io';

class Process {
  var player;
  var monster;
  var map;
  var monsterDefeated;
  var playerDefeated;
  var skipped;
  var healed;

  void load(bool loaded) {
    if(loaded){
      player = Player(true);
    }else{
      player = Player(false);
    }
  }

  void environment() {
    //loop until save and exit
    while (true) {
      // generating map
      map = MapGame();
      // generate stage size
      map.stageLengthSet(player.getLevel);
      print('\n\nMap : ' +
          map.getCurrentMap +
          ' | Stage : ' +
          map.getStageLength.toString());

      // action mode
      Action();

      // when stage ended
      print('End of map ' +
          map.getCurrentMap +
          ' (Stage: ' +
          map.getStageLength.toString() +
          ').');
      print('Do you want to continue to next map?');
      print('1. Continue');
      print('2. Save and Exit');

      // input ended
      var menuExit = int.parse(stdin.readLineSync()!);
      if (menuExit == 1) {
        player.saveData();
        continue;
      } else if (menuExit == 2) {
        player.saveData();
        break;
      } else {
        print('Input mismatch!');
      }
    }
  }

  void Action() {
    // start stage
    while (map.stageContinue()) {
      // show stage
      print('\n\nStage : ' +
          map.getCurrentStage.toString() +
          '/' +
          map.getStageLength.toString() +
          '\n');

      // check this map is last stage if last stage boss will spawn
      if (map.lastStage()) {
        monster = MonsterBoss(player.getLevel);
      } else {
        monster = Monster(player.getLevel);
      }

      // set checker
      monsterDefeated = false;
      playerDefeated = false;
      skipped = false;
      healed = false;

      // Action
      while (!monsterDefeated && !playerDefeated) {
        // show action can do in this turn
        print('\n\nYour action');
        print('1. Attack!');
        if (player.checkSpecialAttackCooldownAvailable()) {
          print('2. Special Attack!');
        } else {
          print('2. Special Attack (Cooldown ' +
              player.getSACrCooldown.toString() +
              '/' +
              player.getSACooldown.toString() +
              ' turn(s)).');
        }
        if (player.checkHealingCooldownAvailable()) {
          print('3. Heal(25% of health)');
        } else {
          print('3. Heal(25% of health) (Cooldown ' +
              player.getHealingCrCooldown.toString() +
              '/' +
              player.getHealingCooldown.toString() +
              ' turn(s)).');
        }
        print('4. Run');

        // loop until action is activated
        while (true) {
          // input
          stdout.write('\n\nYour action : ');
          var action = int.parse(stdin.readLineSync()!);

          // check input
          if (action == 1) {
            print('   Attack!');
            monsterDefeated = monster.defend(player.attack);
            break;
          } else if (action == 2 &&
              player.checkSpecialAttackCooldownAvailable()) {
            print('   Special Attack!');
            monsterDefeated = monster.defend(player.specialAttack());
            break;
          } else if (action == 3 && player.checkHealingCooldownAvailable()) {
            print('   Healing.');
            player.healing();
            healed = true;
            break;
          } else if (action == 4) {
            print('   Run!');
            skipped = true;
            break;
          } else {
            if (action == 2) {
              print('Skill is cooldown!');
            } else {
              print('No action! please input again.');
            }
          }
        }

        // when player skipped player not receive anything
        if (skipped) {
          print('Run to save your life.');
          break;
        }

        // check monster is not defeated
        if (!monsterDefeated) {
          // check player is not heal
          if (!healed) {
            playerDefeated = player.defend(monster.attack);
          }
          // player healed monster turn will skipped
        } else {
          // when monster is defeated
          player.receivingXP(monster.droppedXP(player.getLevel));
          player.getXP;
        }

        // reduce all cooldown
        player.reduceSACooldown();
        player.reduceHealingCooldown();
      }

      // skipped will skip all stage until end of map
      if (skipped) {
        return;
      }

      // when player die
      if (playerDefeated) {
        print('You\'re forgot everything and You are reset to level 1!');
      } else {
        playerDefeated = false;
      }

      // Monster defeated
      print('---------------');
      map.stageUP();
      player.resetCooldown();
    }
  }

  void delay(int time) {
    sleep(Duration(seconds: 0));
  }

  void guide() {
    print('Guide for starter.');
    print('Match begin!');

    map = MapGame();
    map.guideMap();
    guideMap();

    print('\nBattle Guide!');

    player = Player(false);
    player.setName('Hero');
    player.getStats();
    guidePlayer();

    monster = Monster(player.getLevel);
    monster.getStats();
    guideMonster();

    guideFighting();

    guideReceiveXP();

    print('End of guide!');
  }

  void guideReceiveXP() {
    print('\n\nYou\'re got some xp to level up');
    print('When you level up, your stats will increase.');
    player.receivingXP(monster.droppedXP(player.getMaxXP));
  }

  void guideFighting() {
    monsterDefeated = false;
    playerDefeated = false;
    skipped = false;
    healed = false;

    while (!monsterDefeated && !playerDefeated) {
      print('\n\nYour action');
      //attack
      print('1. Attack!');
      //check cooldown of special attack
      if (player.checkSpecialAttackCooldownAvailable()) {
        print('2. Special Attack!');
      } else {
        print('2. Special Attack (Cooldown ' +
            player.getSACrCooldown.toString() +
            '/' +
            player.getSACooldown.toString() +
            ' turn(s)).');
      }
      //check cooldown of healing
      if (player.checkHealingCooldownAvailable()) {
        print('3. Heal(25% of health)');
      } else {
        print('3. Heal(25% of health) (Cooldown ' +
            player.getHealingCrCooldown.toString() +
            '/' +
            player.getHealingCooldown.toString() +
            ' turn(s)).');
      }
      //run
      print('4. Run (Not Available.)');

      guideAction();

      //input until action
      while (true) {
        print('\n\nChoose your action (1-4)');
        stdout.write('\n\nYour action : ');
        var action = int.parse(stdin.readLineSync()!);
        if (action == 1) {
          print('   Attack!');
          monsterDefeated = monster.defend(player.attack);
          break;
        } else if (action == 2 &&
            player.checkSpecialAttackCooldownAvailable()) {
          print('   Special Attack!');
          monsterDefeated = monster.defend(player.specialAttack());
          break;
        } else if (action == 3 && player.checkHealingCooldownAvailable()) {
          print('   Healing.');
          player.healing();
          healed = true;
          break;
        } else {
          if (action == 2) {
            print('Skill is cooldown!');
          } else if (action == 4) {
            print('Run is not available in tutorial!');
          } else {
            print('No action! please input again.');
          }
        }
      }

      //Check monster is defeated?
      if (!monsterDefeated) {
        //monster is not defeated
        if (!healed) {
          //player is not healed
          playerDefeated = player.defend(monster.attack);
        }
        //player is healed attack by monster will skip
      } else {
        //monster is defeated, player will got XP
        player.receivingXP(monster.droppedXP(player.getLevel));
        player.getXP;
      }

      //reduce all cooldown();
      player.reduceSACooldown();
      player.reduceHealingCooldown();
    }
  }

  void guideAction() {
    print('\nAction guide');
    print('You are have 4 actions to do in 1 turn.');
    print('1. Attack is normal damage to monster');
    print('2. Special Attack is heavy damage to monster.');
    print('(cooldown 3 turns)');
    print('3. Heal if your think hp is low you can heal for save your life.');
    print('(cooldown 3 turns and heal 25% of your health)');
    print(
        '4. Run when you think can\'t fight with this monster. You should to run for save life.');
    print('(You can skip 3 times if skip more than 3 stage ');
  }

  void guideMonster() {
    print('About Monster');
    print(
        'HP(health point) if monster hp reach 0. Monster defeated and You will going to next stage.');
    print('ATK(attack point) attack point will increase damage to you.');
    print('DEF(defend point) reducing damage when receiving your damage.');
  }

  void guidePlayer() {
    print('\nPlayer stats!');
    print('HP(health point) if your hp reach 0 you die.');
    print('ATK(attack point) attack point will increase damage to monster.');
    print('DEF(defend point) reducing damage when monster hurting you.');
    delay(2);
    print('\nRemember: You have many life if you have little or many XP.');
    print('Don\'t let XP to zero. If you die, game will reset!');
  }

  void guideMap() {
    print('\n\nMap : ' +
        map.getCurrentMap +
        ' | Stage : ' +
        map.getStageLength.toString());
    print(
        'Game is have many map. \nIn one map have many stage. \nAnd in one stage have only 1 Monster');
  }

  void clearConsole() {
    for (var i = 0; i < 100; i++) {
      print('');
    }
  }
}
