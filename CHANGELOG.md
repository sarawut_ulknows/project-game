# Changelog

## 0.1.0
Project is created and added function to run base game. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Calculate monster hp is reduce from player damage.
- Calculate monster drop xp when defeated.
- Random player stats after receive name from user.
- Calculate player hp when monster attacked.
- Calculate player stats when level up.
- Calculate player hp when healing.
- Calculate player maximum xp to next level when player level up.
- Add guide to first time of game.
- Random map everytime after last map ended.
- Random monster name when player founded.
- Add `FileControl` class to dynamic function.
- Add `PlayerDefault` class to base of player stats.
- Add stage length to map.

## 0.1.1
Update cooldown to `Player` class. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Add cooldown of special attack to player.
- Add cooldown of healing to player.
- Add cooldown default to `PlayerDefault` class.

### Changed
- Move tutorial from `Process` to `UI`.
- Update `CHANGELOG` and `README`.

### Removed
- Remove tutorial in `Process`.

## 0.1.2
Update cooldown to `Process` class. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Reduce cooldown after used 1 turn.
- Add function to check action is available.

### Changed
- Increase turn of `CurrentCooldown` to `Cooldown increase by 1`.

### Removed
- Remove all method of `checkTurnUsed`.

## 0.2.0
Update default stats of player and fix a few bugs. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Function call default value from file to `defaultPlayer`.
- Add error catch when file not found in `defaultPlayer`.

### Fixed
- Fix stage not conitnue when end of stage.
- Fix Monster hp reach zero but not reset in next stage.

## 0.2.1
Update random stats monster by level of player. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Random stats and level of monster.
- Function call default value from file to `defaultMonster`.
- Add error catch when file not found in `defaultMonster`.

### Fixed
- Fix `FileControl` error when finding value of `Stats` and catch other value.
- Fix `level` of monster is random to less than 1.

## 0.3.0
Update boss to the game. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Add boss to the game when player reach last stage.
- Add `default_monster_boss.data` to set default of boss.
- Add `MonsterDefault` class to call default stats of boss.

### Changed
- Change value in code to value called by file default.
- Reduce `baseReduceDMGPercent` to `50`.
- Reduce stats of monster(last change is too hard to defeat or defend.).
- Move all file of `default` to `data` folder.
- Move class of `default` to `character_data` folder.

## 0.3.1
Fix bug xp is not dropped. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Fixed
- Fix xp is dropped by monster is too low.

## 0.4.0
Improve about files. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Add fileStats and stats class to called by other class.

### Changed
- Change some method in player, monster, boss to override by abstract.
- Change file control to mixin for reduce call new object.

## 0.5.0
Add `Save` and `Load` player profile data and improve `Player` class to save and load profile. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Added
- Add `Save` profile data to game.
- Add `Load` profile data to game.

### Changed
- Change save data file from `player.data` to `player_profile.data`

### Fixed
- Fix null exception when loading data from `player_profile.data`.

## 0.5.1
Improve file of `Save` and `Load` profile and update `README.md`. By [@sarawut_ulknows](https://gitlab.com/sarawut_ulknows).

### Changed
- Change file from `LoadPlayer.dart` to `RWProfilePlayer.dart`.

### Removed
- Remove file `SavePlayer.dart`.